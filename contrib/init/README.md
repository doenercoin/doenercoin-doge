Sample configuration files for:

SystemD: doenercoind.service
Upstart: doenercoind.conf
OpenRC:  doenercoind.openrc
         doenercoind.openrcconf
CentOS:  doenercoind.init
OS X:    org.doenercoin.doenercoind.plist

have been made available to assist packagers in creating node packages here.

See doc/init.md for more information.
