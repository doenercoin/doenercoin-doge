
Debian
====================
This directory contains files used to package doenercoind/doenercoin-qt
for Debian-based Linux systems. If you compile doenercoind/doenercoin-qt yourself, there are some useful files here.

## doenercoin: URI support ##


doenercoin-qt.desktop  (Gnome / Open Desktop)
To install:

	sudo desktop-file-install doenercoin-qt.desktop
	sudo update-desktop-database

If you build yourself, you will either need to modify the paths in
the .desktop file or copy or symlink your doenercoin-qt binary to `/usr/bin`
and the `../../share/pixmaps/bitcoin128.png` to `/usr/share/pixmaps`

doenercoin-qt.protocol (KDE)

