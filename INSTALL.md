Building Doenercoin
================

See doc/build-*.md for instructions on building the various
elements of the Doenercoin Core reference implementation of Doenercoin.
